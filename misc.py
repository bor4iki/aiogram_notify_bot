from config import MY_TOKEN
from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from db import SQLighter
import logging

bot = Bot(token=MY_TOKEN)
dp = Dispatcher(bot, storage=MemoryStorage())
logger = logging
logger.basicConfig(level=logging.INFO, filename='bot.log',
                   format='%(levelname)s: %(message)s: %(asctime)s',
                   datefmt='%d/%m %H:%M:%S')
db = SQLighter('bot.db')
