from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup


##########################################################
#  Клавиатура на старт #
inline_kb = InlineKeyboardMarkup(row_width=1)
inline_btn_start_1 = InlineKeyboardButton('Добавить дату события',
                                          callback_data='btn1')
inline_btn_start_2 = InlineKeyboardButton('Добавить напоминание',
                                          callback_data='btn2')

inline_kb.add(inline_btn_start_1)
##########################################################

##########################################################
# Клавиатура на события #
inline_key = InlineKeyboardMarkup(row_width=1)
inline_key_btn_1 = InlineKeyboardButton('Начальное сообщение', callback_data='start')
inline_key_btn_2 = InlineKeyboardButton('Помощь по боту', callback_data='help')
inline_key_btn_3 = InlineKeyboardButton('Добавить новое событие', callback_data='add_note')
inline_key_btn_4 = InlineKeyboardButton('Посмотреть все записи событий', callback_data='view_all_notes')
inline_key_btn_5 = InlineKeyboardButton('Настройки уведомлений', callback_data='settings')
inline_key.add(inline_key_btn_1, inline_key_btn_2, inline_key_btn_3, inline_key_btn_4, inline_key_btn_5)

