from aiogram import types
from misc import dp, bot
from main import add_note, cmd_start, cmd_help, setting_message, view_all_notes


@dp.callback_query_handler(lambda c: c.data == 'btn1')
async def btn1(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    add = add_note(callback_query.message)
    await add


@dp.callback_query_handler(lambda c: c.data == 'btn2')
async def btn2(callback_query: types.CallbackQuery):
    await bot.answer_callback_query(callback_query.id)
    await bot.send_message(callback_query.from_user.id, 'Нажата кнопка добавления напоминания')


@dp.callback_query_handler(lambda c: c.data in ['start', 'help', 'add_note', 'view_all_notes', 'settings'])
async def key_events(call: types.CallbackQuery):
    if call.message:
        if call.data == 'start':
            await bot.answer_callback_query(call.id)
            start = cmd_start(call.message)
            await start
        elif call.data == 'help':
            await bot.answer_callback_query(call.id)
            help_ = cmd_help(call.message)
            await help_
        elif call.data == 'add_note':
            await bot.answer_callback_query(call.id)
            add = add_note(call.message)
            await add
        elif call.data == 'view_all_notes':
            await bot.answer_callback_query(call.id)
            view = view_all_notes(call.message)
            await view
        elif call.data == 'settings':
            await bot.answer_callback_query(call.id)
            settings = setting_message(call.message)
            await settings
