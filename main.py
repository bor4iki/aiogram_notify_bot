from texts import START, HELP
from aiogram import executor, types
from misc import dp, db, bot, logger
from keyboards import inline_kb, inline_key
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher import FSMContext
import callbacks


class AddNote(StatesGroup):
    wait_add_note = State()


@dp.message_handler(commands=['start'])
async def cmd_start(message: types.Message):
    ui = user_id(message)
    first_name = message.chat.first_name
    if not db.check_user(ui):
        db.add_user_db(ui)
        db.add_user_settings(ui)
    logger.info(f'{ui}/{first_name} - нажал /start')
    await message.answer(START, reply_markup=inline_kb)


@dp.message_handler(commands=['help'])
async def cmd_help(message: types.Message):
    ui = user_id(message)
    first_name = message.chat.id
    logger.info(f'{ui}/{first_name} - нажал на /help')
    await message.answer(HELP, parse_mode='html', reply_markup=inline_key)


@dp.message_handler(commands=['view_all_notes'])
async def view_all_notes(message: types.Message):
    ui = user_id(message)
    first_name = message.chat.first_name
    notes = db.get_text_data_hbd(ui)
    mes = ''
    for note in notes:
        id_, text_mes, day, month, year = note
        mes += f'{text_mes}     -     {str(day)}.{str(month)}.{str(year if year != 0 else "")} - /del{id_}\n' \
               f'----------------------------------------------\n'
    if mes == '':
        mes = f'{message.chat.first_name} у тебя нет записей, надо бы добавить.'
    logger.info(f'{ui}/{first_name} - просматривает все записи событий')
    await message.answer(mes, reply_markup=inline_key)


@dp.message_handler(commands=['settings'])
async def setting_message(message):
    ui = user_id(message)
    first_name = message.chat.first_name
    if not db.check_user(ui):
        db.add_user_db(ui)
        db.add_user_settings(ui)
    if not db.check_user_settings(ui):
        db.add_user_settings(ui)
    settings = db.get_user_settings(ui)
    settings = settings[0]
    one_day, three_days, week = settings

    SETTINGS = '<b>Настройки уведомления о ваших датах.</b>\n\n' \
               'На текущий момент ваши настройки:\n\n' \
               '<b>Напомнить за </b> \n' \
               f'день до даты:    {"✅" if one_day == 1 else "⭕️"}   /set_one_day \n' \
               f'3 дня до даты:    {"✅" if three_days == 1 else "⭕️"}   /set_three_days \n' \
               f'неделю до даты:    {"✅" if week == 1 else "⭕️"}   /set_week ️\n\n' \
               '<i>Чтобы изменить настройки уведомления нажмите на команду рядом.</i>'

    logger.info(f'{user_id}/{first_name} - набрал команду /settings')
    await message.answer(SETTINGS, parse_mode='html', reply_markup=inline_key)


@dp.message_handler(commands=['set_one_day'])
async def setting_one_day(message):
    ui = user_id(message)
    first_name = message.chat.first_name
    logger.info(f'{user_id}/{first_name} - изменил настройки уведомления одного дня')
    settings = db.get_user_settings(ui)
    settings = settings[0]
    one_day, three_days, week = settings
    if one_day == 1:
        db.update_user_settings(ui, 0, three_days, week)
        await setting_message(message, reply_markup=inline_key)
    else:
        db.update_user_settings(ui, 1, three_days, week)
        await setting_message(message, reply_markup=inline_key)


@dp.message_handler(commands=['set_three_days'])
async def setting_three_days(message):
    ui = message.chat.id
    first_name = message.chat.first_name
    logger.info(f'{user_id}/{first_name} - изменил настройки уведомления трех дней')
    settings = db.get_user_settings(ui)
    settings = settings[0]
    one_day, three_days, week = settings
    if three_days == 1:
        db.update_user_settings(ui, one_day, 0, week)
        await setting_message(message, reply_markup=inline_key)
    else:
        db.update_user_settings(ui, one_day, 1, week)
        await setting_message(message, reply_markup=inline_key)


@dp.message_handler(commands=['set_week'])
async def setting_week(message):
    ui = message.chat.id
    first_name = message.chat.first_name
    logger.info(f'{user_id}/{first_name} - изменил настройки уведомления недели')
    settings = db.get_user_settings(ui)
    settings = settings[0]
    one_day, three_days, week = settings
    if week == 1:
        db.update_user_settings(ui, one_day, three_days, 0)
        await setting_message(message, reply_markup=inline_key)
    else:
        db.update_user_settings(ui, one_day, three_days, 1)
        await setting_message(message, reply_markup=inline_key)


@dp.message_handler(commands='add_note', state='*')
async def add_note(message: types.Message):
    ui = user_id(message)
    if not db.check_user(ui):
        db.add_user_db(ui)
        db.add_user_settings(ui)
    add_text = f'<b>Добавление новой даты.</b>\n\n' \
               f'Итак, {message.chat.first_name}\n' \
               f'Для того чтобы я запомнил дату и название события,' \
               f' необходимо правильно написать мне строчку\n' \
               f'Следуй примеру ниже, чтобы тебе было легче☺️\n\n' \
               f'<b>21.06.1962 - День рождения В.Цоя 🎸</b>\n\n\n' \
               f'P.s. Если ты случайно нажал на добавление записи и не хочешь ничего добавлять, просто нажми /start'
    await message.answer(add_text, parse_mode='html')
    await AddNote.wait_add_note.set()


@dp.message_handler(state=AddNote.wait_add_note, content_types=types.ContentTypes.TEXT)
async def add_note_step(message: types.Message, state: FSMContext):
    ui = user_id(message)
    first_name = message.chat.first_name
    note = message.text
    if note == '/start':
        logger.info(f'{ui}/{first_name} - вышел из добавления события на /start')
        await cmd_start(message)
        await state.finish()
        return
    try:
        note = note.split('-')
        data = note[0].strip()
        text_event = note[1].strip()
        data = data.split('.')
        day = int(data[0])
        if 1 < day > 31:
            raise Exception
        month = int(data[1])
        if 1 < month > 12:
            raise Exception
        try:
            year = int(data[2])
            if year > 9999:
                raise Exception
        except IndexError:
            year = 0
        db.add_hbd(ui, text_event, day, month, year)

        logger.info(f'{ui}/{first_name} - добавил дату события')
        await message.answer(f'<b>Запись прошла успешно!</b>', parse_mode='html', reply_markup=inline_key)
        await state.finish()
    except Exception as e:
        # print(e)
        logger.warning(f'{ui}/{first_name} - добавил дату с ошибкой {e}')
        await message.answer('<b>Произошла ошибка записи!\n'
                             'Попробуй заново, как показано в инструкции!</b>',
                             parse_mode='html', reply_markup=inline_key)
        await add_note(message)


def user_id(message):
    return message.chat.id


@dp.message_handler(content_types=types.ContentTypes.ANY)
async def all_other_messages(message: types.Message):
    ui = user_id(message)
    first_name = message.chat.first_name
    if 'del' in message.text:
        del_message = message.text[4:]
        db.del_hbd(del_message, ui)
        logger.info(f'{ui}/{first_name} - удалил запись события')
        await message.answer('Запись успешно удалена', reply_markup=inline_key)
        return
    else:
        logger.info(f'{ui}/{first_name} - написал сообщение {message.text}')
        await message.answer('Для справки нажмите /help', reply_markup=inline_key)


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
