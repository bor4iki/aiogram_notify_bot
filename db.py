import sqlite3


class SQLighter:

    def __init__(self, database):
        """Подключаемся к БД и сохраняем курсор соединения"""
        self.connection = sqlite3.connect(database, check_same_thread=False)
        self.cursor = self.connection.cursor()

    def get_all_users(self):
        """Подсчитываем количество юзеров"""
        with self.connection:
            request = self.cursor.execute(
                "SELECT COUNT (*)"
                " FROM User"
            ).fetchall()
            return request

    def get_userid(self):
        """Вывод всех юзер айди"""
        with self.connection:
            request = self.cursor.execute(
                "SELECT user_id "
                "FROM User"
            ).fetchall()
            # print(request)
            return request

    def count_hbd(self):
        """Подсчитываем количество записей о ДР"""
        with self.connection:
            request = self.cursor.execute(
                "SELECT COUNT (*)"
                " FROM HBD"
            ).fetchall()
            return request

    def get_user_status(self, user_id):
        """Юзер проверят статус подписки"""
        with self.connection:
            request = self.cursor.execute(
                "SELECT id, status FROM User WHERE user_id = ?", (user_id,)
            ).fetchall()
            return request

    def check_user_settings(self, user_id):
        with self.connection:
            result = self.cursor.execute(
                "SELECT * FROM Settings "
                "WHERE id_user = (SELECT id FROM User WHERE user_id = ?)",
                (user_id, )
            ).fetchall()
            return bool(len(result))

    def get_user_settings(self, user_id):
        """Проверяем выбор юзера,
         как часто отправлять ему уведомления о празднике"""
        with self.connection:
            request = self.cursor.execute(
                "SELECT one_day, three_days, week"
                " FROM Settings JOIN User ON id = id_user"
                " WHERE user_id = ?", (user_id,)
            ).fetchall()
            return request

    def add_user_settings(self, user_id, one_day=1, three_days=1, week=1):
        """Добавляем настройки уведомления юзера"""
        with self.connection:
            request = self.cursor.execute(
                "INSERT OR IGNORE INTO Settings (id_user, one_day, three_days, week)"
                " VALUES ((SELECT id FROM User WHERE user_id = ?), ?, ?, ?)",
                (user_id, one_day, three_days, week,)
            )
            return request

    def update_user_settings(self, user_id, one_day, three_days, week):
        """Изменение настроек уведомления Юзера"""
        with self.connection:
            return self.cursor.execute(
                "UPDATE Settings"
                " SET one_day = ?, three_days = ?, week = ?"
                " WHERE id_user = (SELECT id FROM User WHERE user_id = ?) ",
                (one_day, three_days, week, user_id)
            )

    def get_text_data_hbd(self, user_id):
        """Даем пользователю всю информацию по его датам"""
        with self.connection:
            request = self.cursor.execute(
                "SELECT HBD.id, name_hbd, day, month, year"
                " FROM HBD JOIN User ON User.id = id_user"
                " WHERE user_id = ?"
                " ORDER BY HBD.id",
                (user_id,)
            ).fetchall()
            return request

    def check_user(self, user_id):
        with self.connection:
            result = self.cursor.execute(
                "SELECT * FROM User "
                "WHERE user_id = ?",
                (user_id, )
            ).fetchall()
            return bool(len(result))

    def add_user_db(self, user_id, status=1):
        """Добавляем юзера в базу данных"""
        with self.connection:
            request = self.cursor.execute(
                "INSERT INTO User (user_id, status)"
                " VALUES(?, ?)",
                (user_id, status,)
            )
            return request

    def add_hbd(self, user_id, text_hbd, day, month, year=0):
        """Добавление происшествия в БД"""
        with self.connection:
            return self.cursor.execute(
                "INSERT INTO HBD (id_user, name_hbd, day, month, year)"
                " VALUES ((SELECT id FROM User WHERE user_id = ?), ?, ?, ?, ?)",
                (user_id, text_hbd, day, month, year,)
            )

    def get_month(self):
        """Узнаём данные занесенные в таблциу HBD"""
        with self.connection:
            request = self.cursor.execute(
                "SELECT User.user_id,name_hbd, day, month, year "
                "FROM HBD "
                "JOIN User ON HBD.id_user = User.id "
            ).fetchall()
            return request

    def del_hbd(self, id_hdb, user_id):
        """Удаляем строку с не нужным праздником"""
        with self.connection:
            request = self.cursor.execute(
                "DELETE FROM HBD "
                "WHERE HBD.id = ? AND (SELECT User.id FROM User WHERE user_id = ?) ",
                (id_hdb, user_id,)
            )
            return request

    def close(self):
        """Закрываем соединение с БД"""
        self.connection.close()


if __name__ == '__main__':
    bd = SQLighter('bot.db')
    # bd.get_all_users()
    # bd.add_user_db(1221)
    # bd.add_user_settings(1221)
    # bd.update_user_settings(1743, 1, 1, 1)
    # print(bd.get_user_settings(1221))
    # bd.add_hbd(1216, "test4", 2, 12, 1996)
    bd.get_text_data_hbd(474301121)
    # bd.get_user_status(1216)
    # bd.del_hbd(1, 643585749)
    # bd.get_userid()
